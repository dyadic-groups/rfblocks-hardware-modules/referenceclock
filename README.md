# Low Phase Noise Reference Clock

## Features

- Very low phase noise: -145 dBc/Hz at 1 kHz offset (100 MHz carrier).
- Selectable on-board or external 10 MHz reference.
- Nominal 8 dBm output power.

## Documentation

Full documentation for the reference clock is available at
[RF Blocks](https://rfblocks.org/boards/Reference-Clock.html)

## License

[CERN-OHL-W v2.](https://ohwr.org/project/cernohl/wikis/Documents/CERN-OHL-version-2)
